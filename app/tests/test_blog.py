import pytest
from app.db import get_db


def test_index(client, auth):
    response = client.get('/')
    assert b"Chat App" in response.data

    auth.login()
    response = client.get('/')
    assert b'Posts' in response.data

def test_delete(client, auth, app):
    auth.login()
    response = client.post('/1/delete')
    
    with app.app_context():
        db = get_db()
        db.cursor().execute('SELECT * FROM post WHERE id = 1;')
        try:
            post = db.cursor().fetchone()
        except:
            post = None
        
        assert post is None