import os
import tempfile
import json

import pytest
from app import create_app
from app.db import get_db, init_db, close_db

with open(os.path.join(os.path.dirname(__file__), 'data.sql'), 'rb') as f:
    _data_sql = f.read().decode('utf8')


@pytest.fixture
def app():
    filename = os.path.abspath("tests/metatestdb.json")
    with open(filename, 'r') as f:
        datastore = json.load(f)

    POSTGRES = {
        'user': datastore["user"],
        'pw': datastore["password"],
        'db': datastore["dbname"],
        'host': datastore["host"],
        'port': datastore["port"],
    }

    app = create_app({
        'TESTING': True,
        'DATABASE': 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES,
    })

    with app.app_context():
        init_db()
        get_db().cursor().execute(_data_sql)
        get_db().commit()

    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()

class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login(self, username='test', password='test'):
        return self._client.post(
            '/auth/login',
            data={'username': username, 'password': password}
        )

    def logout(self):
        return self._client.get('/auth/logout')


@pytest.fixture
def auth(client):
    return AuthActions(client)