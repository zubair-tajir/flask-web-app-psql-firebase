import pytest
from flask import g, session
from app.db import get_db

def test_login(client, auth):
    assert client.get('/auth/login').status_code == 200

@pytest.mark.parametrize(('username', 'password', 'message'), (
    ('a', 'test', b'Incorrect username.'),
    ('test', 'a', b'Incorrect password.'),
))
def test_login_validate_input(auth, username, password, message):
    response = auth.login(username, password)
    with pytest.raises(AssertionError) as e:
        assert message in response.data

def test_logout(client, auth):
    auth.login(True)

    with client:
        auth.logout()
        assert 'user_id' not in session
