import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from app.db import get_db
from psycopg2 import sql, extras, connect as db_connect

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/login', methods=('GET', 'POST'))
def login(test=False):
    if request.method == 'POST' and request.is_json:
        uid = request.json['uid']
        name = request.json['name']
        email = request.json['email']
        phone = request.json['phone']

        db = get_db()
        
        if name is None:
            name = 'user'

        check = False
        with db.cursor(cursor_factory=extras.DictCursor) as cursor:
            query = 'SELECT * FROM "user" WHERE id = %s;'
            cursor.execute(query, (uid,))
            user = cursor.fetchone()
            check = True if user is None else False
        
        if check:
            with db.cursor() as cursor:
                username = ''
                query = 'INSERT INTO "user" (id, username, displayname) VALUES (%s, %s, %s);'
                if phone is None:
                    username = email
                else:
                    username = phone
                cursor.execute(query, (uid, username, name))
                db.commit()
                query = 'SELECT * FROM "user" WHERE id = %s;'
                cursor.execute(query, (uid,))
                user = cursor.fetchone()
        
        session.clear()
        session['user_id'] = user['id']
        return redirect(url_for('index'))

    if test:
        session.clear()
        session['user_id'] = 1
        return redirect(url_for('index'))
        
    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        db = get_db()
        with db.cursor(cursor_factory=extras.DictCursor) as cursor:
            query = 'SELECT * FROM "user" WHERE id = %s;'
            cursor.execute(query, (user_id,))
            g.user = cursor.fetchone()

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
